export type BuildMode = 'production' | 'development';

export interface BuildPaths {
  entry: string;
  html: string;
  output: string;
  public: string

}

export interface BuildOptions {
  port: number;
  paths: BuildPaths
  mode: BuildMode
  verbose: boolean
  platform: BuildPlatform
}

type Verbose = 'true' | 'false'; // bool
export type BuildPlatform = 'mobile' | 'desktop';

export interface EnvVariables {
  mode?: BuildMode;
  port?: string;
  verbose?: Verbose;
  platform?: BuildPlatform;
}



