import type { Configuration as DevServerConfiguration } from "webpack-dev-server";
import { BuildOptions } from "./types";


export const webpackDevServer = (opts: BuildOptions):DevServerConfiguration => {
  return {
      port: opts.port,
      open: true,
      historyApiFallback: true,
      hot: true
    }
}