import { Configuration, DefinePlugin } from "webpack"
import { BuildOptions } from "./types"
import HtmlWebpackPlugin from 'html-webpack-plugin';
// import webpack from 'webpack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import ReactRefreshWebpackPlugin from "@pmmmwh/react-refresh-webpack-plugin";
import path from "path";
import CopyPlugin from "copy-webpack-plugin";

export const webpackPlugins = (opts: BuildOptions): Configuration['plugins'] => {
  const isDev = opts.mode === 'development';
  const isProd = opts.mode === 'production';

  const plugins: Configuration['plugins'] = [
    new DefinePlugin({
      _PLATFORM_: JSON.stringify(opts.platform),
      // _DEVELOPMENT_: opts.mode === 'development',
      // _PRODUCTION_: opts.mode === 'production',
    }),
    new HtmlWebpackPlugin({ template: opts.paths.html, favicon: path.resolve(opts.paths.public, 'favicon.ico') }),
  ]

  if (isDev) {
    plugins.push(new ForkTsCheckerWebpackPlugin())
    plugins.push(new ReactRefreshWebpackPlugin())
  }

  if (isProd) {
    plugins.push(new MiniCssExtractPlugin({
      filename: 'css/[name].[contenthash:8].css',
      chunkFilename: 'css/[name].[contenthash:8].css',
    }))
    plugins.push(new CopyPlugin({
      patterns: [
          { from: path.resolve(opts.paths.public, 'locales'), to: path.resolve(opts.paths.output, 'locales') },
      ],
  }),)
  }


  return plugins;
}