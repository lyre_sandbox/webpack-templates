// #Delarations

declare module '*.scss' {
  const content: Record<string, string>;
  export default content;
}
declare module '*.css' {
  const content: Record<string, string>;
  export default content;
}

declare const _PLATFORM_: 'mobile' | 'desktop'
// declare const _DEVELOPMENT_: boolean
// declare const _PRODUCTION_: boolean