import webpack from "webpack";
import { BuildOptions } from "./types";
import { webpackPlugins } from "./webpack-plugins";
import { webpackLoaders } from "./webpack-loaders";
import { webpackDevServer } from "./webpack-dev-server";

export const webpackBuild = (opts: BuildOptions) => {
  const isDev = opts.mode === 'development';

  return ({
    mode: opts.mode,
    entry: opts.paths.entry,
    optimization: {
      minimize: !opts.verbose,
    },
    output: {
      path: opts.paths.output,
      filename: '[name].[contenthash].js',
      clean: true
    },
    plugins: webpackPlugins(opts),
    module: {
      rules: webpackLoaders(opts),
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js', '.css','.scss'],
    },
    cache: true,
    devtool: isDev && 'inline-source-map',
    devServer: isDev ? webpackDevServer(opts) : undefined
  } satisfies webpack.Configuration)
}