import { ModuleOptions } from "webpack"
import { BuildOptions } from "./types"
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import ReactRefreshTypeScript from "react-refresh-typescript";

export const webpackLoaders = (opts: BuildOptions): ModuleOptions['rules'] => {
  const isDev = opts.mode === 'development';
  const isProd = opts.mode === 'production';

  // const assetLoader = {
  //     test: /\.(png|jpg|jpeg|gif)$/i,
  //     type: 'asset/resource',
  // }

  // const svgrLoader = {
  //     test: /\.svg$/i,
  //     use: [
  //         {
  //             loader: '@svgr/webpack',
  //             options: {
  //                 icon: true,
  //                 svgoConfig: {
  //                     plugins: [
  //                         {
  //                             name: 'convertColors',
  //                             params: {
  //                                 currentColor: true,
  //                             }
  //                         }
  //                     ]
  //                 }
  //             }
  //         }
  //     ],
  // }


  const cssLoaderWithModules = {
      loader: "css-loader",
      options: {
          modules: {
            localIdentName: '[name]_[local]_[hash:base64:3]',
          },
      },
  }

  const scssLoader = {
      test: /\.s[ac]ss$/i,
      use: [
        isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
          // Translates CSS into CommonJS
          cssLoaderWithModules,
          // Compiles Sass to CSS
          "sass-loader",
      ],
  }



  const tsLoader = {
      exclude: /node_modules/,
      test: /\.tsx?$/,
      use: [
          {
              loader: 'ts-loader',
              options: {
                  transpileOnly: isDev,
                  getCustomTransformers: () => ({
                      before: [isDev && ReactRefreshTypeScript()].filter(Boolean),
                  }),
              }
          }
      ]
  }

  // const babelLoader = buildBabelLoader(options);


  return [
      // assetLoader,
      scssLoader,
      tsLoader,
      // babelLoader,
      // svgrLoader
  ]

}