import path from 'path';

import { EnvVariables } from './config/types';
import { webpackBuild } from './config/webpack-build';


export default (env: EnvVariables) => {
  const pathR = (str: string) => path.resolve(__dirname, str)

  const config = webpackBuild({
    paths: {
      entry: pathR('src/main.tsx'),
      output: pathR('build'),
      html: pathR('public/index.html'),
      public: pathR('public')
    },
    mode: env.mode === 'production' ? 'production' : 'development',
    port: parseInt(env.port || '') || 3005,
    verbose: env.verbose === 'true',
    platform: env.platform === 'mobile' ? 'mobile' : 'desktop'
  });

  return config;
}